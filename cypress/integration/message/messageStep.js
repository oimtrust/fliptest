import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps'
import MessagePage from './messagePage'

Given('Message box opened', () => {
    MessagePage.visit()
})

When('I click {string} in message confirmation box', () => {
    MessagePage.confirm()
})

When('I click {string} in message confirmation box', () => {
    MessagePage.cancel()
})

Then('I redirect to homepage with confirmation message {string}', () => {
    MessagePage.message()
})