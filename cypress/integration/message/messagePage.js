/// <reference types="Cypress" />

const URL            = 'https://your-url.com'
const YES_BUTTON     = 'button[name="confirm_button"]'
const CANCEL_BUTTON  = 'button[name="cancel_button"]'
const MESSAGE_CLASS  = 'div[class="message-box"]'

class MessagePage {
    static visit() {
        cy.visit(URL)
    }

    static confirm() {
        cy.get(YES_BUTTON).click()
    }

    static cancel() {
        cy.get(CANCEL_BUTTON).click()
    }

    static message() {
        cy.get(MESSAGE_CLASS).should('contain.text', 'Permintaan anda akan di proses besok hari antara pukul 07.00-20.00 WIB')
    }
}

export default MessagePage