Feature: Message Confirmation box

    Message confirmation box for transaction money from A Bank to B Bank 
    with different country or the same country with different bank

    Scenario: Click "Yakin" button
        Given Message box opened
        When I click "Yakin" in message confirmation box
        Then I redirect to homepage with confirmation message "Permintaan anda akan di proses besok hari antara pukul 07.00-20.00 WIB"

    Scenario: Click "Batal" button
        Given Message box opened
        When I click "Batal" in message confirmation box
        Then I redirect to homepage with confirmation message "Permintaan anda akan di proses besok hari antara pukul 07.00-20.00 WIB"